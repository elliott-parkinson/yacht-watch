import fetch from "cross-fetch";
import * as cheerio from "cheerio";

export class Cheerio {
    public static async FetchSiteDocument(url: string): Promise<CheerioStatic> {
        /* Get Listing */
        const response: Response = await fetch(url);

        if (response.status >= 400) {
            throw new Error("Bad response from server. Code: " + response.status);
        }

        /* Parse Listing */
        const html: string = await response.text();
        const $: CheerioStatic = await cheerio.load(html);

        return $;
    }
}