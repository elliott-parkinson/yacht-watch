import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity()
export class FetchCacheEntity {
    @PrimaryColumn()
    id: number;

    @Column()
    url: string;

    @Column()
    markup: string;
}