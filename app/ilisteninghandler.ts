import { BoatDetails } from "./";

export interface IListeningHandler {
    Name(): string;
    CanProcessHostName(hostname: string): boolean;

    GetListingDetails(url: string): Promise<BoatDetails>;
}