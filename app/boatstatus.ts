export enum BoatStatus {
    Removed = "Removed",
    Unknown = "Unknown",
    Available = "Available",
    UnderOffer = "Under Offer",
    Sold = "Sold",
}
