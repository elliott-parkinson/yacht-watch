import { BoatStatus } from "./";

export class BoatDetails {
    public heading: string;


    public make: string;
    public model: string;
    public price: string;
    public berths: number;
    public year: number;

    public status: BoatStatus;
    public description: string;
    public location: string;

    public images: string[] = [];
    public knownUrls: string[] = [];
}