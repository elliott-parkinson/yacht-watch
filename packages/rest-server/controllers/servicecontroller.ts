
import { Controller, Get, Req } from '@nestjs/common';

@Controller('service')
export class ServiceController {
    @Get()
    public findAll(@Req() request): string {
        return 'This is a test response';
    }
}