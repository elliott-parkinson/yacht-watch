
import { Module } from '@nestjs/common';
import { ServiceController } from './controllers/servicecontroller';

@Module({
  controllers: [ServiceController],
})
export class ApplicationModule {}