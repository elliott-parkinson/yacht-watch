
import { BoatStatus, BoatDetails, IListeningHandler, Cheerio } from "../../app";
import { Helpers } from "./helpers";

export class Boatshed implements IListeningHandler {
    public Name(): string {
        return "Boatshed";
    }

    public CanProcessHostName(hostname: string): boolean {
        return hostname.indexOf("boatshed.com") !== -1 ? true : false;
    }

    public async GetListingDetails(url: string): Promise<BoatDetails> {
        const $: CheerioStatic = await Cheerio.FetchSiteDocument(url);

        $(".recommended-container").remove();
        $(".alert").remove();

        const specifications: any = await Helpers.getDetails($);

        const boatDetails: BoatDetails = new BoatDetails();
        boatDetails.heading = $("h1").text().replace(/\r?\n|\r/g, " ").trim();

        // boatDetails.description = Helpers.getDescription($);
        // boatDetails.make = specifications.Make;
        // boatDetails.model = specifications.Model;
        boatDetails.berths = specifications.Berths;
        boatDetails.year = specifications.Year;
        boatDetails.price = Helpers.filterPrice(specifications.Price);
        boatDetails.location = specifications.Lying;
        boatDetails.status = Helpers.parseStatus(specifications.Price);
        // boatDetails.images = Helpers.getImages($);
        boatDetails.knownUrls = [url];

        return boatDetails;
    }
}