import { BoatStatus, BoatDetails } from "../../app";


export class Helpers {
    public static parseStatus(status: string): BoatStatus {
        if (!status) {
            return BoatStatus.Unknown;
        }

        if (status.toLowerCase().indexOf("sold") !== -1) {
            return BoatStatus.Sold;
        }

        if (status.toLowerCase().indexOf("offer") !== -1) {
            return BoatStatus.UnderOffer;
        }

        return BoatStatus.Available;
    }

    public static async getDetails($: CheerioStatic): Promise<any> {
        const specificationsElements: any = $("div[itemtype=\"http://schema.org/Product\"] > .row > div:nth-of-type(2) .row");
        const specifications: any = {};
        await specificationsElements.each( async (index: number, element: CheerioElement) => {
            specifications[ await $("div:nth-of-type(1)", element).first().text().replace(/[\W_]+/g,"").trim()] =
                    await $("div:nth-of-type(2)", element).text().replace(/\r?\n|\r/g, " ").trim();
        });

        return specifications;
    }

    public static getImages($: CheerioStatic): string[] {
        const imageElements: any = $(".swiper-wrapper img");
        const images: string[] = [];
        imageElements.each( (index: number, element: CheerioElement) => {
            images.push( $(element).attr("data-src") );
        });

        return images;
    }


    public static filterPrice(price: string): string {
        if (price.indexOf("/") !== -1) {
            return "Unknown";
        }
        return price ? price : "Unknown";
    }
}