import { BoatStatus, BoatDetails, IListeningHandler, Cheerio } from "../../app";
import { Helpers } from "./helpers";


export class ApolloDuck implements IListeningHandler {
    public Name(): string {
        return "Apollo Duck";
    }

    public CanProcessHostName(hostname: string): boolean {
        return hostname.indexOf("apolloduck.com") !== -1 || hostname.indexOf("apolloduck.co.uk") !== -1? true : false;
    }

    public async GetListingDetails(url: string): Promise<BoatDetails> {
        const $: CheerioStatic = await Cheerio.FetchSiteDocument(url);
        const specifications: any = Helpers.getSpecifications($);

        /* Generate Response Object */
        const boatDetails: BoatDetails = new BoatDetails();
        boatDetails.heading = $(".advert .advertTitle").text().replace(/\r?\n|\r/g, " ").trim();
        boatDetails.description = Helpers.getDescription($);
        boatDetails.make = specifications.Make;
        boatDetails.model = specifications.Model;
        boatDetails.berths = specifications.Berths;
        boatDetails.year = specifications.Yearconstructed;
        boatDetails.price = Helpers.filterPrice(specifications.Price);
        boatDetails.location = Helpers.filterLocation(specifications.Location);
        boatDetails.status = Helpers.parseStatus(specifications.Status);
        boatDetails.images = Helpers.getImages($);
        boatDetails.knownUrls = [url, specifications.Website];

        return boatDetails;
    }
}