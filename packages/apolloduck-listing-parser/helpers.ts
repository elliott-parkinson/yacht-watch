import { BoatStatus, BoatDetails } from "../../app";


export class Helpers {
    public static parseStatus(status: string): BoatStatus {
        if (!status) {
            return BoatStatus.Unknown;
        }

        switch (status.toLowerCase()) {
            case "available":
                return BoatStatus.Available; break;

            case "under offer":
                return BoatStatus.UnderOffer; break;

            case "sold":
                return BoatStatus.Sold; break;

            default: return BoatStatus.Unknown;
        }
    }

    public static getSpecifications($: CheerioStatic): any {
        const specificationsElements: any = $(".featureSpecLine");
        const specifications: any = {};
        specificationsElements.each( (index: number, element: CheerioElement) => {
            specifications[ $(".featureSpecLabel", element).text()
                .replace(/[\W_]+/g,"").trim()] =
                    $(".featureSpecData", element).text().replace(/\r?\n|\r/g, " ").trim();
        });

        return specifications;
    }

    public static getImages($: CheerioStatic): string[] {
        const imageElements: any = $(".swiper-wrapper img");
        const images: string[] = [];
        imageElements.each( (index: number, element: CheerioElement) => {
            images.push( $(element).attr("data-src") );
        });

        return images;
    }

    public static getDescription($: CheerioStatic): string {
        const description: any = $(".featureSection.cp p");

        return description.text();
    }

    public static filterPrice(price: string): string {
        return price ? price.substring(0, price.indexOf("|")).trim() : "Unknown";
    }

    public static filterLocation(location: string): string {
        return location ? location.replace("[View Map]", "").trim() : "Unknown";
    }
}