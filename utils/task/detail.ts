import { Container, Service, Inject } from "typedi";

import * as program from "commander";
import * as colors from "colors";
import { URL } from "url";

import { BoatStatus, BoatDetails, IListeningHandler } from "../../app";

import { ApolloDuck } from "../../packages/apolloduck-listing-parser";
import { Boatshed } from "../../packages/boatshed-listing-parser";


program
    .command("detail <url>")
    .action(async (url: string) => {
        const listingUrl: string = url;
        const hostname: string = new URL(listingUrl).hostname;

        console.log(colors.grey("[detail] ") + colors.cyan(listingUrl)  + colors.white(" - fetching details."));

        let processor: ListingProcessor = new ListingProcessor([new ApolloDuck(), new Boatshed()]);

        
        let handler: IListeningHandler | null = processor.GetHandlerForHostname(hostname);

        if (handler) {
            console.log(colors.grey("[detail] ") + colors.cyan(handler.Name())  + colors.white(" - recognised handler."));

            const details: BoatDetails = await handler.GetListingDetails(listingUrl);
            const view: ConsoleRenderer = new ConsoleRenderer();
            view.RenderListingDetails(details);
        }
        else {
            console.log(colors.grey("[detail] ") + colors.red("No handler found for the given url."));
        }
    });


export class ListingProcessor {å
    private listeningHandlers: IListeningHandler[];

    constructor(listeningHandlers: IListeningHandler[]) {
        this.listeningHandlers = listeningHandlers ? listeningHandlers : [];
    }

    public GetHandlerForHostname(hostname: string): IListeningHandler | null {
        let handlers: IListeningHandler[] = this.listeningHandlers.filter( (handler: IListeningHandler) => handler.CanProcessHostName(hostname) );

        return handlers.length ? handlers[0] : null;
    }
}


export class ConsoleRenderer {
    public RenderListingDetails(details: BoatDetails) {
        console.log(colors.grey("[detail] ") + colors.green(details.heading));

        let statusText: string = "";
        switch (details.status) {
            case BoatStatus.Unknown: statusText = colors.grey(BoatStatus[details.status]); break;
            case BoatStatus.Removed: statusText = colors.red(BoatStatus[details.status]); break;
            case BoatStatus.Available: statusText = colors.green(BoatStatus[details.status]); break;
            case BoatStatus.UnderOffer: statusText = colors.yellow(BoatStatus[details.status]); break;
            case BoatStatus.Sold: statusText = colors.red(BoatStatus[details.status]); break;
        }
        console.log(colors.grey("[detail] ") + colors.blue(details.year.toString())
            + colors.white(" | ") + colors.blue(details.price)
            + colors.white(" | ") + colors.blue(details.location)
            + colors.white(" | ") + statusText
        );
    }
}